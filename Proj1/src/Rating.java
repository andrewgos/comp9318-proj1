
public class Rating {
	
	private int ratingID;
	private int userID;
	private int itemID;
	private int rating;
	
	public Rating(int ratingID, int userID, int itemID, int rating){
		this.setRatingID(ratingID);
		this.setUserID(userID);
		this.setItemID(itemID);
		this.setRating(rating);
	}

	public int getRatingID() {
		return ratingID;
	}
	public int getUserID() {
		return userID;
	}
	public int getItemID() {
		return itemID;
	}
	public int getRating() {
		return rating;
	}

	
	public void setRatingID(int ratingID) {
		this.ratingID = ratingID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public void setItemID(int itemID) {
		this.itemID = itemID;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}

	

	
	
	
	
}
