import java.io.*;
import java.util.*;

public class RPTrees {
	
	private static int numOfPoint;
	private static int pointDimensionality;
	private static int numOfTrees;
	private static int maxSizeOfLeaf;
	private static Scanner gaussianFileScanner;


	public static void main(String[] args) throws FileNotFoundException {
		numOfPoint = Integer.parseInt(args[0]); //number of item = n = 20
		pointDimensionality = Integer.parseInt(args[1]); //d = 3 
		numOfTrees = Integer.parseInt(args[2]); // =  3
		maxSizeOfLeaf = Integer.parseInt(args[3]); // = 3
		String dataFilePath = args[4]; 
		String randomGaussianFilePath = args[5];
		String queryFilePath = args[6];
		
		File randomGaussianFile = new File(randomGaussianFilePath); 
		gaussianFileScanner = new Scanner(randomGaussianFile);
		
		// INITIALIZING MATRIX Q
		double[][] matrixQ = new double[pointDimensionality][numOfPoint];
		File matrixFile = new File(dataFilePath);
        Scanner matrixFileScanner = new Scanner(matrixFile);
		int row = 0;
        while(matrixFileScanner.hasNextLine()){
        	String line = matrixFileScanner.nextLine();
        	String[] lineFields = line.split(" ");
        	
        	for(int col = 0; col<numOfPoint; col++){
        		matrixQ[row][col] = Double.parseDouble(lineFields[col]);
        	}
        	row++;
        }
        

        // SET UP MATRIX S
    	ArrayList<ItemPoint> matrixS = new ArrayList<ItemPoint>();        
    	for(int itemCounter = 0; itemCounter < numOfPoint;itemCounter++){
    		Vector<Double> itemVector = new Vector<Double>();
    		for(int dimensionCounter = 0; dimensionCounter < pointDimensionality; dimensionCounter++){
    			itemVector.add(matrixQ[dimensionCounter][itemCounter]);
    		}
    		ItemPoint newItemPoint = new ItemPoint(itemCounter+1, itemVector);
    		matrixS.add(newItemPoint);
    	}
        
        // GENERATE EACH TREES
        ArrayList<Node> arrayOfTrees = new ArrayList<Node>();
        for(int treeCounter= 0; treeCounter<numOfTrees;treeCounter++){
        	Node rootNode = makeTree(matrixS);
        	arrayOfTrees.add(rootNode);
        	//printLeafNodes(rootNode);
        	//System.out.println("\n");
        }
		
        
        //READING QUERY FILE
        File queryFile = new File(queryFilePath);
        Scanner queryFileScanner = new Scanner(queryFile);
		//LOOP EACH QUERY
        while(queryFileScanner.hasNextLine()){
        	ArrayList<EdgeDistance> totalGatheredNeighbour = new ArrayList<EdgeDistance>();
        	int query = Integer.parseInt(queryFileScanner.nextLine());
        	ItemPoint queryItem = null;
        	// FIND QUERY'S ITEMPOINT OBJECT
        	for(ItemPoint itemIter : matrixS){
        		if(itemIter.getItemID() == query){
        			queryItem = itemIter;
        		}
        	}
        	// GET NN IN EACH TREES
        	for(Node tree : arrayOfTrees){
        		ArrayList<EdgeDistance> gatheredNeighbour = findNN(tree,queryItem);
        		// PUT ALL NEIGHBOUR INTO FINAL ARRAY
        		for(EdgeDistance neighbour : gatheredNeighbour){
        			boolean neighbourAlreadyExists = false;
        			// CHECK IF ALREADY EXIST
        			for(EdgeDistance checkNeighbour : totalGatheredNeighbour){
        				if(checkNeighbour.getTo().getItemID() == neighbour.getTo().getItemID()){
        					neighbourAlreadyExists = true;
        					break;
        				}
        			}
        			if(!neighbourAlreadyExists){
        				totalGatheredNeighbour.add(neighbour);	
        			}
        		}
        	}
        	
        	//GET THE TOP NN FROM ALL THE TREES
        	Collections.sort(totalGatheredNeighbour, new DistanceComparator());
        	int topNNCounter = 0;
        	int topNNLimit = 3;
        	while(topNNCounter < topNNLimit && topNNCounter<totalGatheredNeighbour.size()){
        		System.out.print(totalGatheredNeighbour.get(topNNCounter).getTo().getItemID());
        		topNNCounter++;
        		if(topNNCounter != topNNLimit){
        			System.out.print(" ");
        		}
        	}
        	if(queryFileScanner.hasNextLine()){
        		System.out.print("\n");
        	}
        }
        
        
		gaussianFileScanner.close();
		matrixFileScanner.close();
		queryFileScanner.close();
	}
	
	private static Node makeTree(ArrayList<ItemPoint> inputMatrixS){
		Node newNode = new Node();

		// CREATE LEAF NODE IF TRUE
		if(inputMatrixS.size() <= maxSizeOfLeaf){
			newNode.setLeafValues(inputMatrixS);
			return newNode;
		}
		// GET THE RULE
		newNode.setRule(chooseRule(inputMatrixS));
		
		// SEPARATE MATRIX INTO LEFT AND RIGHT
		ArrayList<ItemPoint> leftValues = new ArrayList<ItemPoint>();
		ArrayList<ItemPoint> rightValues = new ArrayList<ItemPoint>();
		for (ItemPoint itemPoint : inputMatrixS){
			double value = dotProduct(itemPoint.getItemVector(), newNode.getRule().getRandomGaussian());
			if(newNode.getRule().checkRule(value) == true){
				leftValues.add(itemPoint);
			}
			else{
				rightValues.add(itemPoint);
			}
		}
		
		// RECURSE TO LEFT AND RIGHT
		newNode.setLeftChild(makeTree(leftValues));
		newNode.setRightChild(makeTree(rightValues));
		
		return newNode;
	}
	
	private static Rule chooseRule(ArrayList<ItemPoint> S){
		ArrayList<ItemPoint> clonedS = (ArrayList<ItemPoint>) S.clone();
		
		// GET THE 3 GAUSSIAN VALUES
		Vector<Double> randomGaussian = new Vector<Double>();
		for(int dimensionCounter = 0; dimensionCounter < pointDimensionality; dimensionCounter++){
			double test = Double.parseDouble(gaussianFileScanner.nextLine());
			randomGaussian.add(test);
		}
		
		ArrayList<Double> ruleArray = new ArrayList<Double>();
		
		// TIMES ALL WITH RANDOM GAUSSIAN NUMBER
		for(ItemPoint itemPoint : clonedS){
			ruleArray.add(dotProduct(itemPoint.getItemVector(), randomGaussian));
		}
		
		// SORT THE VALUES
		Collections.sort(ruleArray);
		
		// GET THE MEDIAN AS THE RULE
		double medianValue = 0;
		if(clonedS.size() % 2 == 0){ // IF EVEN
			int middleIndex = (clonedS.size()/2);
			medianValue = (ruleArray.get(middleIndex-1)+ruleArray.get(middleIndex))/2;
		}
		else{ // IF ODD
			int middleIndex = (clonedS.size()/2);
			medianValue = ruleArray.get(middleIndex);
		}
		
		Rule newRule = new Rule(medianValue, randomGaussian);
		return newRule;
		
	}
	
	private static Double dotProduct(Vector<Double> vector, Vector<Double> randomGaussian){
		Double result = 0.0;
		for(int dimensionCounter = 0; dimensionCounter<pointDimensionality; dimensionCounter++){
			result += vector.get(dimensionCounter) * randomGaussian.get(dimensionCounter);
		}
		return result;
	}
	
	
	public static void printLeafNodes(Node node){
		if(node == null){
			return;
		}
		if(node.getLeftChild() == null && node.getRightChild()==null){
			System.out.print("{");
			for(ItemPoint ip : node.getLeafValues()){
				System.out.print(ip.getItemID());
				if(node.getLeafValues().indexOf(ip) != node.getLeafValues().size()-1){
					System.out.print(", ");  
				}
			}
			System.out.print("} ");
		}
		printLeafNodes(node.getLeftChild()); 
		printLeafNodes(node.getRightChild());      
	}
	
	
	
	public static ArrayList<EdgeDistance> findNN(Node node, ItemPoint query){
		if(node == null){
			return null;
		}
		
		//IF THIS IS THE LEAF NODE
		if(node.getLeftChild() == null && node.getRightChild()==null){
			ArrayList<ItemPoint> leafValueArray = node.getLeafValues();
			ArrayList<EdgeDistance> gatheredNeighbourDistance = new ArrayList<EdgeDistance>();
			
			// FIND ALL NEIGHBOUR
			for(ItemPoint leafPoint : leafValueArray){
				if(leafPoint.getItemID() != query.getItemID()){ // IF NEIGHBOUR
					//CALCULATE THE DISTANCE FROM THE VECTOR
					EdgeDistance edge = new EdgeDistance(query, leafPoint);
					gatheredNeighbourDistance.add(edge);
				}
			}
			return gatheredNeighbourDistance;
		}
		
		// DECIDES WHERE TO RECURSE NEXT
		double dotProductResult = dotProduct(query.getItemVector(), node.getRule().getRandomGaussian());
		if(node.getRule().checkRule(dotProductResult) == true){
			return findNN(node.getLeftChild(), query);
		}
		else{
			return findNN(node.getRightChild(), query);
		}
	}
	
	
	public static class DistanceComparator implements Comparator<EdgeDistance> {
        public int compare(EdgeDistance o1, EdgeDistance o2) {
            if (o1.getDistance() > o2.getDistance()) return 1;
            if (o2.getDistance() > o1.getDistance()) return -1;
            return 0;
        }
    } 
	

	
}
