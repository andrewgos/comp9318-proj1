import java.util.ArrayList;


public class Node {
	

	private Rule rule;
	private Node leftChild;
	private Node rightChild;
	private ArrayList<ItemPoint> leafValues;
	
	public Node(){
		this.setRule(null);
		this.setLeftChild(null);
		this.setRightChild(null);
		this.setLeafValues(null);
	}

	public Rule getRule() {
		return rule;
	}
	public Node getLeftChild() {
		return leftChild;
	}
	public Node getRightChild() {
		return rightChild;
	}
	public ArrayList<ItemPoint> getLeafValues() {
		return leafValues;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}
	public void setLeftChild(Node leftChild) {
		this.leftChild = leftChild;
	}
	public void setRightChild(Node rightChild) {
		this.rightChild = rightChild;
	}
	public void setLeafValues(ArrayList<ItemPoint> leafValues) {
		if(leafValues != null){
			this.leafValues = (ArrayList<ItemPoint>) leafValues.clone();
		}
		else{
			this.leafValues = null;
		}
	}

	

	
	
	
	
}
