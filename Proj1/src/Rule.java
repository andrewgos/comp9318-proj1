import java.util.*;


public class Rule {

	private double rule;
	private Vector<Double> randomGaussian;
	
	public Rule(double rule, Vector<Double> randomGaussian){
		this.setRuleValue(rule);
		this.setRandomGaussian(randomGaussian);
	}
	
	
	public boolean checkRule(double value){
		if(value <= rule){
			return true;
		}
		else{
			return false;
		}
	}

	public double getRuleValue() {
		return rule;
	}
	public Vector<Double> getRandomGaussian() {
		return randomGaussian;
	}
	
	public void setRuleValue(double rule) {
		this.rule = rule;
	}
	public void setRandomGaussian(Vector<Double> randomGaussian) {
		this.randomGaussian = randomGaussian;
	}




	




	
	
}
