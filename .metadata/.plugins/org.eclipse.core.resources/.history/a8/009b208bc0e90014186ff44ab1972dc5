import java.io.*;
import java.util.*;

import javax.swing.text.html.MinimalHTMLWriter;


public class RPTrees {
	
	private static int numOfPoint;
	private static int pointDimensionality;
	private static int numOfTrees;
	private static int maxSizeOfLeaf;
	private static Scanner gaussianFileScanner;


	public static void main(String[] args) throws FileNotFoundException {
		numOfPoint = Integer.parseInt(args[0]); //number of item = n = 20
		pointDimensionality = Integer.parseInt(args[1]); //d = 3 
		numOfTrees = Integer.parseInt(args[2]); // =  3
		maxSizeOfLeaf = Integer.parseInt(args[3]); // = 3
		String dataFilePath = args[4]; 
		String randomGaussianFilePath = args[5];
		String queryFilePath = args[6];
		
		File randomGaussianFile = new File(randomGaussianFilePath); 
		gaussianFileScanner = new Scanner(randomGaussianFile);
		
		// INITIALIZING MATRIX Q
		double[][] matrixQ = new double[pointDimensionality][numOfPoint];
		File matrixFile = new File(dataFilePath);
        Scanner matrixFileScanner = new Scanner(matrixFile);
		int row = 0;
        while(matrixFileScanner.hasNextLine()){
        	String line = matrixFileScanner.nextLine();
        	String[] lineFields = line.split(" ");
        	
        	for(int col = 0; col<numOfPoint; col++){
        		matrixQ[row][col] = Double.parseDouble(lineFields[col]);
        	}
        	row++;
        }
        

        
        
        // SET UP MATRIX S
    	ArrayList<ItemPoint> matrixS = new ArrayList<ItemPoint>();        
    	
    	for(int itemCounter = 0; itemCounter < numOfPoint;itemCounter++){
    		Vector<Double> itemVector = new Vector<Double>();
    		for(int dimensionCounter = 0; dimensionCounter < pointDimensionality; dimensionCounter++){
    			itemVector.add(matrixQ[dimensionCounter][itemCounter]);
    		}
    		ItemPoint newItemPoint = new ItemPoint(itemCounter+1, itemVector);
    		matrixS.add(newItemPoint);
    	}
        
        // GENERATE EACH TREES
        ArrayList<Node> arrayOfTrees = new ArrayList<Node>();
        for(int treeCounter= 0; treeCounter<numOfTrees;treeCounter++){
        	Node rootNode = makeTree(matrixS);
        	arrayOfTrees.add(rootNode);
        	printLeafNodes(rootNode);
        	System.out.println("\n");
        }
		
        
        //READING QUERY FILE
        File queryFile = new File(queryFilePath);
        Scanner queryFileScanner = new Scanner(queryFile);
		
        while(queryFileScanner.hasNextLine()){
        	Hashtable<Integer, Double> finalGatheredNeighbour = new Hashtable<Integer, Double>();
        	int query = Integer.parseInt(queryFileScanner.nextLine());
        	ItemPoint queryItem = null;
        	
        	// FIND QUERY'S ITEMPOINT OBJECT
        	for(ItemPoint itemIter : matrixS){
        		if(itemIter.getItemID() == query){
        			queryItem = itemIter;
        		}
        	}
        	for(Node tree : arrayOfTrees){
        		Hashtable<Integer, Double> gatheredNeighbour = findNN(tree,queryItem);
        		for(Double neighbour : gatheredNeighbour){
        			
        		}
        		finalGatheredNeighbour.putAll(gatheredNeighbour);
        	}
        	
        	
        	
        }
        
        
        
		gaussianFileScanner.close();
		matrixFileScanner.close();
		queryFileScanner.close();
	}
	
	private static Node makeTree(ArrayList<ItemPoint> inputMatrixS){
		Node newNode = new Node();

		if(inputMatrixS.size() <= maxSizeOfLeaf){
			newNode.setLeafValues(inputMatrixS);
			return newNode;
		}
		newNode.setRule(chooseRule(inputMatrixS));
		
		ArrayList<ItemPoint> leftValues = new ArrayList<ItemPoint>();
		ArrayList<ItemPoint> rightValues = new ArrayList<ItemPoint>();
		
		for (ItemPoint itemPoint : inputMatrixS){
			double value = dotProduct(itemPoint.getItemVector(), newNode.getRule().getRandomGaussian());
			if(newNode.getRule().checkRule(value) == true){
				leftValues.add(itemPoint);
			}
			else{
				rightValues.add(itemPoint);
			}
		}
		
		newNode.setLeftChild(makeTree(leftValues));
		newNode.setRightChild(makeTree(rightValues));
		
		return newNode;
	}
	
	private static Rule chooseRule(ArrayList<ItemPoint> S){
		ArrayList<ItemPoint> tempS = (ArrayList<ItemPoint>) S.clone();
		
		// GET THE 3 GAUSSIAN VALUES
		Vector<Double> randomGaussian = new Vector<Double>();
		for(int dimensionCounter = 0; dimensionCounter < pointDimensionality; dimensionCounter++){
			double test = Double.parseDouble(gaussianFileScanner.nextLine());
			randomGaussian.add(test);
		}
		
		ArrayList<Double> ruleArray = new ArrayList<Double>();
		
		// TIMES ALL WITH RANDOM GAUSSIAN NUMBER
		for(ItemPoint itemPoint : tempS){
			ruleArray.add(dotProduct(itemPoint.getItemVector(), randomGaussian));
		}
		
		// SORT THE VALUES
		Collections.sort(ruleArray);
		
		// GET THE MEDIAN AS THE RULE
		double medianValue = 0;
		if(tempS.size() % 2 == 0){ // IF EVEN
			int middleIndex = (tempS.size()/2);
			medianValue = (ruleArray.get(middleIndex-1)+ruleArray.get(middleIndex))/2;
		}
		else{ // IF ODD
			int middleIndex = (tempS.size()/2);
			medianValue = ruleArray.get(middleIndex);
		}
		
		Rule newRule = new Rule(medianValue, randomGaussian);
		
		return newRule;
		
	}
	
	private static Double dotProduct(Vector<Double> vector, Vector<Double> randomGaussian){
		Double result = 0.0;
		for(int dimensionCounter = 0; dimensionCounter<pointDimensionality; dimensionCounter++){
			result += vector.get(dimensionCounter) * randomGaussian.get(dimensionCounter);
		}
		return result;
		
	}
	
	

	public static void printLeafNodes(Node node){
		if(node == null){
			return;
		}
		if(node.getLeftChild() == null && node.getRightChild()==null){
			System.out.print("{");
			for(ItemPoint ip : node.getLeafValues()){
				System.out.print(ip.getItemID());
				if(node.getLeafValues().indexOf(ip) != node.getLeafValues().size()-1){
					System.out.print(", ");  
				}
			}
			System.out.print("} ");
		}
		printLeafNodes(node.getLeftChild()); 
		printLeafNodes(node.getRightChild());      
	}
	
	
	
	public static Hashtable<Integer, Double> findNN(Node node, ItemPoint query){
		if(node == null){
			return null;
		}
		
		//IF THIS IS THE LEAF NODE
		if(node.getLeftChild() == null && node.getRightChild()==null){
			ArrayList<ItemPoint> leafValueArray = node.getLeafValues();
			Hashtable<Integer, Double> gatheredNeighbourDistance = new Hashtable<Integer, Double>();
			
			// FIND ALL NEIGHBOUR
			for(ItemPoint leafValue : leafValueArray){
				if(leafValue.getItemID() != query.getItemID()){ // IF NEIGHBOUR
					double distance = 0;
					double temp = 0;
					//CALCULATE THE DISTANCE FROM THE VECTOR
					for(int dimensionCounter=0;dimensionCounter<pointDimensionality;dimensionCounter++){
						temp = leafValue.getItemVector().get(dimensionCounter)-query.getItemVector().get(dimensionCounter);
						temp = Math.pow(temp, 2);
						distance += temp;
					}
					distance = Math.sqrt(distance);
					gatheredNeighbourDistance.put(leafValue.getItemID(), distance);
				}
			}
			
			return gatheredNeighbourDistance;
		}
		
		// DECIDES WHERE TO RECURSE NEXT
		double dotProductResult = dotProduct(query.getItemVector(), node.getRule().getRandomGaussian());
		if(node.getRule().checkRule(dotProductResult) == true){
			return findNN(node.getLeftChild(), query);
		}
		else{
			return findNN(node.getRightChild(), query);
		}
		 
		    
	}
	public static void sortValue(Hashtable<?, Double> t){

	       //Transfer as List and sort it
	       ArrayList<Map.Entry<?, Double>> l = new ArrayList<Map.Entry<?, Double>>(t.entrySet());
	       Collections.sort(l, new Comparator<Map.Entry<?, Double>>(){

	         public int compare(Map.Entry<?, Double> o1, Map.Entry<?, Double> o2) {
	            return o1.getValue().compareTo(o2.getValue());
	        }});

	       System.out.println(l);
	    }
	
	
	
	
}
